import * as THREE from "three";

/**
 * Initiate a sceen for threeJS and let you easily add meshes to it.
 *
 * # Example
 * ```
 * const scene = new ThreeScene()
 * scene.addMesh(...)
 * ```
 */
class ThreeScene {
	constructor(
		fov = 75,
		aspect = undefined,
		near = 0.1,
		far = 1000,
		position = { x: 0, y: 0, z: 5 }
	) {
		// Instanciate the scene
		this._scene = new THREE.Scene();

		// Get size and set default aspect
		this.size = this.getSize();
		if (!aspect) {
			aspect = this.size.width / this.size.height;
		}

		// Default camera
		this._camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
		this._camera.position.x = position.x;
		this._camera.position.y = position.y;
		this._camera.position.z = position.z;

		// Renderer
		this._renderer = new THREE.WebGLRenderer();
		this._renderer.setSize(this.size.width, this.size.height);
		document.body.appendChild(this._renderer.domElement);

		// Auto resize the sceen when window is resized.
		this._initOnResize(this._camera, this._renderer);

		// Let's animate the scene
		this._animate();

		// The list of all meshes into the sceen
		this._meshes = [];
	}
	/**
	 * Update when window resize.
	 * @private
	 */
	_initOnResize(camera, renderer) {
		window.addEventListener(
			"resize",
			() => {
				this.size = this.getSize();
				camera.aspect = this.size.ratio;
				camera.updateProjectionMatrix();
				renderer.setSize(this.size.width, this.size.height);
			},
			false
		);
	}

	/**
	 * Run the animation request to animate the scene.
	 * @private
	 */
	_animate() {
		requestAnimationFrame(this._animate.bind(this));
		this._renderer.render(this._scene, this._camera);
	}

	/**
	 * Get the size and the ratio aspect of the window
	 * @returns {object} - {width,height,ratio}
	 */
	getSize() {
		return {
			width: window.innerWidth,
			height: window.innerHeight,
			ratio: window.innerWidth / window.innerHeight,
		};
	}

	/**
	 * Add a mesh to the sceen.
	 * @param {ThreeMesh} threeMesh - A ThreeMesh to be added into the sceen.
	 */
	addMesh(threeMesh) {
		this._scene.add(threeMesh.get());
		this._meshes.push(threeMesh);
	}
}

export default ThreeScene;
