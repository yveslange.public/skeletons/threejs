import * as THREE from "three";

/**
 * A Three Mesh that can set its own animation
 */
class ThreeMesh {
	constructor(geometry, material, mesh, animation) {
		this.geometry = geometry ? geometry : new THREE.BoxGeometry();
		this.material = material
			? material
			: new THREE.MeshBasicMaterial({ color: 0x00ff00 });
		this.mesh = mesh ? mesh : new THREE.Mesh(this.geometry, this.material);
		this.setAnimation(animation);
	}

	/**
	 * Animate the cube by applying a simple rotation
	 */
	_animate(mesh) {
		requestAnimationFrame(this._animate.bind(this, this.mesh));
		this.animation(mesh, this.geometry, this.material);
	}

	/**
	 * Sets the animation of the mesh
	 */
	setAnimation(animation) {
		this.animation = animation;
		if (this.animation) {
			this._animate(this.mesh);
		}
	}

	/**
	 * Returns the mesh object
	 * @returns {ThreeMesh}
	 */
	get() {
		return this.mesh;
	}
}

export default ThreeMesh;
