import { ThreeScene, ThreeMesh } from "./libs";

// Example
const scene = new ThreeScene();

// Generate a mesh and add it to the scene
const mesh = new ThreeMesh();
scene.addMesh(mesh);

// Create and set the animation of the mesh
mesh.setAnimation((mesh) => {
	mesh.rotation.x += 0.01;
	mesh.rotation.y += 0.02;
});
